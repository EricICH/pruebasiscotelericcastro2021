class User < ApplicationRecord
	validates :name, presence: true
	validates :nid, presence: true
	validates :name, format: { with: /\A[a-zA-Z]+\z/, message: "Solo Se Permiten Letras en El Campo Nombre" }
	validates :nid, numericality: { only_integer: true }
	validates :main_tlf, numericality: { only_integer: true }
	validates :sec_tlf, numericality: { only_integer: true }
	validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
end
