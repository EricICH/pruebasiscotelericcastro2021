json.extract! user, :id, :user_type, :nid, :date_e, :date_v, :name, :email, :main_tlf, :sec_tlf, :created_at, :updated_at
json.url user_url(user, format: :json)
