class ChangeColumnAttr < ActiveRecord::Migration[6.1]
  def change
  	change_column :users, :main_tlf, :string, limit: 20
  	change_column :users, :sec_tlf, :string, limit: 20
  end
end
