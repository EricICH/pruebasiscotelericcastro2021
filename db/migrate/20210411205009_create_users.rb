class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :type
      t.integer :nid
      t.date :date_e
      t.date :date_v
      t.string :name
      t.string :email
      t.integer :main_tlf
      t.integer :sec_tlf

      t.timestamps
    end
  end
end
