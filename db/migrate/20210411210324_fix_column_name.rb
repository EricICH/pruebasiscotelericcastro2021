class FixColumnName < ActiveRecord::Migration[6.1]
  def change
  	rename_column :Users, :type, :user_type
  end
end
